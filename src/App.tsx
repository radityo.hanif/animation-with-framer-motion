import React from 'react';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'react-toastify/dist/ReactToastify.css';
import './styles/global.css';

import NotFoundPage from './pages/error/404.tsx';
import Home from './pages/home.tsx';

export const Context = React.createContext([])

function App() {
  return (
    <>
      <ToastContainer />
      <Router>
        <Routes>
          {/* Public Pages */}
          <Route path='/' element={<Home />} />

          {/* Error Pages */}
          <Route path='*' element={<NotFoundPage />} />
        </Routes>
      </Router>
    </>
  )
}

export default App
