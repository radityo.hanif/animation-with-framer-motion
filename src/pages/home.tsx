import { Box, Heading, Stack } from "@chakra-ui/react";
import { motion } from 'framer-motion'
import PublicLayout from "../layout/public-layout";

export default function Home() {
  return (
    <PublicLayout>
      <Box className={'bg-glass'} height={'80vh'} width={'80vw'} p={5} borderRadius={'2xl'}>
        <Stack gap={10}>
          <Heading color={'whiteAlpha.900'} textAlign={'center'}>
            Play with Framer Motion
          </Heading>

          <Stack direction={'row'} justifyContent={'center'}>
            <motion.div
              animate={{
                opacity: [0, 1],
                scale: [0, 1],
                rotate: [180, 0],
                rotateY: [0, 0, -180, 0]
              }}
            >
              <Box bg={'whiteAlpha.800'} p={10}>
                Box 1
              </Box>
            </motion.div>

            <motion.div
              animate={{
                opacity: [0, 0.5, 1, 0.5, 0],
              }}
              transition={{
                duration: 1,
                repeat: Infinity
              }}
            >
              <Box bg={'whiteAlpha.800'} p={10}>
                Box 2
              </Box>
            </motion.div>

            <motion.div
              animate={{
                rotateX: [0, 50, 100, 0],
                rotateY: [0, 50, -50, 0]
              }}
              transition={{
                duration: 1.2
              }}
            >
              <Box bg={'whiteAlpha.800'} p={10}>
                Box 3
              </Box>
            </motion.div>

            <motion.div
              animate={{
                opacity: [0, 1],
                x: [-900, 0]
              }}
              transition={{
                duration: 1.5
              }}
            >
              <Box bg={'whiteAlpha.800'} p={10}>
                Box 4
              </Box>
            </motion.div>
          </Stack>

          <Stack>
            <motion.div
              animate={{
                opacity: [0.5, 1, 0.5],
              }}
              transition={{
                duration: 1.5,
                repeat: Infinity,
                ease: 'linear'
              }}
            >
              <Heading color={'whiteAlpha.900'} fontWeight={'medium'} textAlign={'center'}>
                Sample Text Animation
              </Heading>
            </motion.div>
          </Stack>
        </Stack>
      </Box>
    </PublicLayout>
  )
}